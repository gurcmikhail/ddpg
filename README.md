# DDPG
1. Скачиваем и устанавливаем Anaconda по ссылке:

https://www.anaconda.com/products/individual

Все настройки можно оставить по умолчанию.

2. Устанавливаем Tensorflow. Для этого открываем программу Anaconda Prompt и вводим команду:

conda install -c anaconda tensorflow

3. Запускаем установившийся Jupyter Notebook. Открываем наш скрипт.